---
# Copyright (C) 2020 CipherMail B.V. <https://www.ciphermail.com/>
# Copyright (C) 2020 DebOps <https://debops.org/>
# SPDX-License-Identifier: GPL-3.0-or-later

openvpn__base_packages: ['openvpn', 'libpam-yubico']
openvpn__packages: []

openvpn__dev: 'tun'
openvpn__port: 1194
openvpn__keepalive: '10 60'
openvpn__tls_version_min: '1.2'
openvpn__tls_cipher: '{{ "TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:" +
                         "TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256" }}'
openvpn__ecdh_curve: 'prime256v1'
openvpn__ca: '/etc/pki/realms/domain/CA.crt'
openvpn__cert: '/etc/pki/realms/domain/default.crt'
openvpn__key: '/etc/pki/realms/domain/default.key'
openvpn__auth_gen_token: 86400
openvpn__duplicate_cn: true
openvpn__ncp_disable: false
openvpn__cipher: 'AES-128-GCM'
openvpn__auth: 'SHA-256'

openvpn__servers:

  - name: 'ansible'
    options: |
      topology subnet
      server 10.88.153.0 255.255.255.0
      server-ipv6 fd2a:5696:8981::/64
      ifconfig-pool-persist ansible-ipp.txt
      status ansible-status.log
      push "redirect-gateway def1 ipv6"

openvpn__yubico_id: '{{
  lookup("file", secret + "/yk-val/api.yubico.com/id") }}'
openvpn__yubico_key: '{{
  lookup("file", secret + "/yk-val/api.yubico.com/key") }}'
openvpn__yubico_debug: false
openvpn__yubico_nullok: false
openvpn__yubico_token_id_length: 12
openvpn__yubico_urllist:
  - 'https://api.yubico.com/wsapi/2.0/verify'
  - 'https://api2.yubico.com/wsapi/2.0/verify'
  - 'https://api3.yubico.com/wsapi/2.0/verify'
  - 'https://api4.yubico.com/wsapi/2.0/verify'
  - 'https://api5.yubico.com/wsapi/2.0/verify'

openvpn__ldap_cacertfile: '/etc/ssl/certs/ca-certificates.crt'
openvpn__ldap_uri: '{{ "ldaps://" + ansible_local.ldap.hosts[0]
                                    | d("ldap." + ansible_domain) }}'

openvpn__ldap_user_attr: 'uid'
openvpn__ldap_yubi_attr: 'yubiKeyId'
openvpn__ldap_yubi_attr_prefix: ''

openvpn__ldap_device_dn: '{{ ansible_local.ldap.device_dn | d([]) }}'

openvpn__ldap_peopledn: '{{ ansible_local.ldap.people_rdn + ","
                            + openvpn__ldap_base
                            if (ansible_local.ldap.people_rdn | d())
                            else "" }}'

openvpn__ldap_filter: '(&
                         (objectClass=inetOrgPerson)
                         (objectClass=yubiKeyUser)
                         (uid=%u)
                         (|
                           (authorizedService=all)
                           (authorizedService=openvpn)
                         )
                       )'
openvpn__ldap_self_rdn: 'uid=openvpn'
openvpn__ldap_self_object_classes: ['account', 'simpleSecurityObject']
openvpn__ldap_self_attributes:
  uid: '{{ openvpn__ldap_self_rdn.split("=")[1] }}'
  userPassword: '{{ openvpn__ldap_bind_password }}'
  host: '{{ [ansible_fqdn, ansible_hostname] | unique }}'
  description: >-
    Account used by the OpenVPN service to access the LDAP directory
openvpn__ldap_starttls: true
openvpn__ldap_bind_dn: '{{ ([openvpn__ldap_self_rdn]
                            + openvpn__ldap_device_dn) | join(",") }}'
openvpn__ldap_bind_password: '{{
  lookup("password", secret + "/ldap/credentials/"
                     + openvpn__ldap_bind_dn | to_uuid
                     + ".password chars=ascii_letters,digits length=22") }}'
openvpn__ldap_base: '{{ ansible_local.ldap.basedn
                        | d("dc=" + ansible_domain.split(".")
                                    | join(",dc=")) }}'

openvpn__ferm_dependent_rules:

  - name: 'openvpn'
    by_role: 'debops.openvpn'
    type: 'accept'
    protocols: ['udp']
    dport: ['openvpn']

  - name: 'openvpn_forwarding'
    by_role: 'debops.openvpn'
    chain: 'FORWARD'
    type: 'accept'
    interface: 'tun0'

openvpn__ldap__dependent_tasks:

  - name: '{{ "Create OpenVPN account for "
              + openvpn__ldap_device_dn | join(",") }}'
    dn: '{{ openvpn__ldap_bind_dn }}'
    objectClass: '{{ openvpn__ldap_self_object_classes }}'
    attributes: '{{ openvpn__ldap_self_attributes }}'
    no_log: '{{ debops__no_log | d(true) }}'
